/* global describe, it, APP, sinon */

(function() {
	'use strict';

	describe('Array', function() {
		describe('#indexOf()', function() {
			it('should return -1 when the value is not present', function() {
				[1, 2, 3].indexOf(5).should.equal(-1);
				[1, 2, 3].indexOf(0).should.equal(-1);
			});
		});
	});

	describe('APP', function() {
		it('sum function', function() {
			APP.sum(1, 3).should.equal(4);
		});
	});

	describe('async call', function() {
		it('test spy', function() {

			var callback = sinon.spy();
	        APP.getCommentsFor("/some/article", callback);
	        expect(callback.calledOnce).to.be.true;
		});
	});

	describe('ajax', function() {
		beforeEach(function() {

			this.xhr = sinon.useFakeXMLHttpRequest();
	        var requests = this.requests = [];
	        this.xhr.onCreate = function (xhr) {
	            requests.push(xhr);
	        };
		});

		afterEach(function() {
			this.xhr.restore();
		});

		it('get feed success', function() {

			var callback = sinon.spy();
	        APP.getFeed().done(callback);

	        this.requests.length.should.equal(1);
	        this.requests[0].respond(200, { "Content-Type": "application/json" },
	                                 '[{ "id": 12, "comment": "Hey there" }]');
	        expect( callback.calledWith([{ id: 12, comment: "Hey there" }]) ).to.be.true;
		});

		it('get feed fail', function() {

			var callback = sinon.spy(function(error){
				expect(error.responseJSON).to.eql({success:false});
			});
	        APP.getFeed().fail(callback);

	        this.requests.length.should.equal(1);
	        this.requests[0].respond(404, { "Content-Type": "application/json" },
	                                 '{"success":false}');
	        expect( callback.calledOnce ).to.be.true;
		});
	});

})();